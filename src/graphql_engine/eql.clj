(ns graphql-engine.eql)

(declare lacinia-selection-tree->eql)

(defn- tree->eql-reducer [s k v]
  (if-let [inner-tree (:selections (first v))]
    (conj s {k (lacinia-selection-tree->eql inner-tree)})
    (conj s k)))

(defn lacinia-selection-tree->eql
  ([selection-tree]
   (reduce-kv tree->eql-reducer [] selection-tree))
  ([selection-tree args]
   (let [eql (lacinia-selection-tree->eql selection-tree)
         query-namespace (namespace (ffirst selection-tree))
         namespaced-args (mapcat (fn [[k v]]
                                   [(keyword query-namespace (name k)) v]) args)]
     [{(vec namespaced-args)
       eql}])))