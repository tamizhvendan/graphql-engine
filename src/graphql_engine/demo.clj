(ns graphql-engine.demo
  (:require [graphql-engine.lacinia :as lacinia]
            [io.pedestal.http :as http]
            [jdbc.assistant.postgresql :as postgres]
            [com.walmartlabs.lacinia.pedestal :as lacinia-pedestal]))

(defn start [config db-spec]
  (let [service (-> (lacinia/schema config (postgres/->Postgresql db-spec))
                    (lacinia-pedestal/service-map {:graphiql true})
                    http/create-server)]
    (http/start service)
    service))

(defn stop [service]
  (http/stop service))

(comment
  (def db-spec {:dbtype "postgres"
                :dbname "dvdrental"
                :user "postgres"
                :password. "postgres"})
  (def config {:hodur {:type {:metadata {:lacinia/tag-recursive true}}}})
  (def service (start config db-spec))
  (stop service))