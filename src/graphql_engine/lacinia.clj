(ns graphql-engine.lacinia
  (:require [jdbc-hodur.core :as jdbc-hodur]
            [jdbc-hodur.metadata.query :as mdq]
            [inflections.core :as inf]
            [com.walmartlabs.lacinia.executor :as executor]
            [com.walmartlabs.lacinia.util :as lacinia-util]
            [hodur-lacinia-schema.core :as hodur-lacinia]
            [clojure.jdbc.metadata :as jdbc-metadata]
            [jdbc.assistant.core :as assistant]
            [graphql-engine.eql :as eql]
            [com.walmartlabs.lacinia.schema :as schema]))

(defn resolver-map [assistable hodur-schema]
  {:resolver (fn [context args value]
               (let [sel-tree (executor/selections-tree context)
                     q (eql/lacinia-selection-tree->eql sel-tree args)]
                 (spit "test.edn" q)
                 (assistant/query-single assistable hodur-schema q)))})

(defn- primitive-type-conversion [type]
  (case type
    :Integer :Int
    (symbol (name type))))

(defn- pk-column->arg [{col-name :name type :type}]
  {(keyword (inf/camel-case col-name :lower))
   {:type (list 'non-null (primitive-type-conversion type))}})

(defn- pk-table->query [{:keys [type primary-keys columns]}]
  (let [pks (set primary-keys)
        pk-columns (filter #(pks (:name %)) columns)]
    {(-> (name type) (inf/camel-case :lower) keyword)
     {:type (symbol (name type))
      :args (apply merge (map pk-column->arg pk-columns))
      :resolve :resolver}}))

(defn- pk-table-queries [hodur-schema]
  (->> (mdq/pk-tables hodur-schema)
       (map pk-table->query)
       (apply merge)))

(defn schema [config assistable]
  (let [db-metadata (jdbc-metadata/fetch (:db-spec assistable))
        hodur-schema (jdbc-hodur/hodur-schema config db-metadata)]
    (-> (hodur-lacinia/schema hodur-schema)
        (assoc :queries (pk-table-queries hodur-schema))
        (lacinia-util/attach-resolvers (resolver-map assistable hodur-schema))
        schema/compile)))

(comment
  (require '[datascript.core :as d])
  (def hodur-schema (jdbc-hodur/fetch-hodur-schema config db-spec))
  (d/q '[:find ?type ?primary-keys ?column-name (pull ?field-type [*])
         :where
         [?t :jdbc-hodur/tag true]
         [?t :jdbc.metadata.relation/type :table]
         [?t :type/PascalCaseName ?type]
         [?t :jdbc.metadata.relation/name ?table-name]
         [?t :jdbc.metadata.relation/schema ?table-schema]
         [?t :jdbc.metadata.relation/primary-keys ?primary-keys]
         [?c :jdbc.metadata.column/table-name ?table-name]
         [?c :jdbc.metadata.column/table-schema ?table-schema]
         [?c :jdbc.metadata.column/name ?column-name]
         [?c :field/type ?field-type]
         [?c :jdbc.metadata.column/jdbc-data-type _]] @hodur-schema))