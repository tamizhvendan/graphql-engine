(defproject graphql-engine "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [com.walmartlabs/lacinia "0.33.0"]
                 [hodur/lacinia-schema "0.1.2" :exclusions [hodur/engine]]
                 [clojure.jdbc.metadata "0.1.0-SNAPSHOT"]
                 [jdbc-hodur "0.1.0-SNAPSHOT"]
                 [jdbc.assistant.core "0.1.0-SNAPSHOT"]]
  :profiles {:test {:dependencies [[com.walmartlabs/lacinia-pedestal "0.12.0"]
                                   [org.postgresql/postgresql "42.2.5"]
                                   [jdbc.assistant.postgresql "0.1.0-SNAPSHOT" :exclusions [jdbc-hodur jdbc.assistant.core]]]}})
